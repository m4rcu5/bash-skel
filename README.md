# Bash Skel
Bash script template with some built in functions and config file support.

## Functions
* bs_log
* bs_output

### bs_log
Writes a string to `$BS_LOGFILE`. The strings will be tagged as follows:    

* `[message]` when none of the options below is used
* `[ok]` when the string starts with **"ok: "**
* `[warning]` when the string starts with **"warn: "**
* `[error]` when the string starts with **"error: "**

Log entry example:
`01-01-17:12:07:19 [error] - Script must be run by root`

### bs_output
Wrapper for `echo -e` to colorize terminal output. Acts a lot like `bs_log` regarding message tags.    

* Starting the string with **"ok: "** will result in **green** output
* Starting the string with **"warn: "** will result in **yellow** output
* Starting the string with **"error: "** will result in **red** output
