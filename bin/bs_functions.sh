function bs_output {
    local status="${1%%:*}"
    local msg="${*:(2)}"
    case ${status} in
        "ok" ) echo -e "-> \e[0;32m${msg}\e[0m";;
        "warn" ) echo -e "-> \e[0;33m${msg}\e[0m";;
        "error" ) echo -e "-> \e[0;31m${msg}\e[0m";;
        * ) echo -e "${*}"
    esac
}
export -f bs_output

function bs_log {
	local timestamp=$(date +%d-%m-%y:%H:%M:%S)
	local status="${1%%:*}"
    local msg="${*:(2)}"
    case ${status} in
        "ok" ) echo -e "${timestamp} [ok] - ${msg}" >> ${BS_LOGFILE};;
        "warn" ) echo -e "${timestamp} [warning] - ${msg}" >> ${BS_LOGFILE};;
        "error" ) echo -e "${timestamp} [error] - ${msg}" >> ${BS_LOGFILE};;
        * ) echo -e "${timestamp} [message] - ${*}" >> ${BS_LOGFILE}
    esac
}
if [[ -w ${BS_LOGFILE} ]]; then
	export -f bs_log
else
	echo "Log file ${BS_LOGFILE} is not writable"
	exit 1
fi
