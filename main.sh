#!/bin/bash
## Set a beacon
declare -x BS_ROOT="$(dirname $(realpath $0))"
## Include the config file
source "$BS_ROOT/config/config.sh"
## Include Bash Skel functions
source "$BS_ROOT/bin/bs_functions.sh"

## The main function. Constructor, if you will.
function main {
    mychecks
    
    ## Simple demo of config file and the ouput and log functions
    local msg="warn: This was in env during script execution: BS_SOMEVALUE=${BS_SOMEVALUE}"
    bs_log ${msg}
    bs_output ${msg}
}

## Probably wise to have some checks in a wrapper
function mychecks {
    # Simple root check example
    if [[ "$(id -u)" != "0" ]]; then
        local msg='error: Script must be run by root'
        bs_log ${msg}
        bs_output ${msg}
        exit 1
    fi
}

main ${*}