## Puts values in the environment of the
## main script's shell. 
declare -x BS_SOMEVALUE='1337'
declare -x BS_LOGDIR="${BS_ROOT}/log"
declare -x BS_LOGFILE="${BS_LOGDIR}/main.log"